import gi

gi.require_version('Gtk', "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Intimate Diary")
        self.set_default_size(640, 480)

        self.grid = Gtk.Grid()
        self.add(self.grid)

        self.create_toolbar()
        self.create_textview()

    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        self.grid.attach(toolbar, 0, 0, 3, 1)

        button_cal = Gtk.ToolButton()
        button_cal.set_icon_name("x-office-calendar-symbolic")
        button_cal.connect("clicked", self.on_cal_clicked)
        toolbar.insert(button_cal, 0)

        button_save = Gtk.ToolButton()
        button_save.set_icon_name("document-save-symbolic")
        button_save.connect("clicked", self.on_save_clicked)
        toolbar.insert(button_save, 1)

        button_search = Gtk.ToolButton()
        button_search.set_icon_name("system-search-symbolic")
        button_search.connect("clicked", self.on_search_clicked)
        toolbar.insert(button_search, 2)

    def on_cal_clicked(self, widget):
        print("youyou")

    def on_save_clicked(self, widget):
        """ save action """
        txt_buffer = self.textview.get_buffer()
        txt_buffer_start = txt_buffer.get_start_iter()
        txt_buffer_end = txt_buffer.get_end_iter()
        txt_buffer_content = txt_buffer.get_text(txt_buffer_start, txt_buffer_end, True)
        file = open("test.txt", "w")
        file.write(txt_buffer_content)
        file.close()
        print("saved to test.txt")

    def on_search_clicked(self, widget):
        print("searching for youyou")

    def create_textview(self):
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_hexpand(True)
        scrolled_window.set_vexpand(True)
        self.grid.attach(scrolled_window, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        self.textview.set_wrap_mode(Gtk.WrapMode.WORD)
        self.textbuffer = self.textview.get_buffer()
        self.textbuffer.set_text(
            "No one's ever understood me like you, Tom... \nI'm so glad I've got this diary to confide in... \n" +
            "It's like a friend I can carry around in my pocket...")
        scrolled_window.add(self.textview)


win = MainWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()

Gtk.main()
