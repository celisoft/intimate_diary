Intimate Diary
==============

What is intimate diary ?
------------------------

The goal of intimate diary is to provide a personal diary with GPG encryption in order to avoid other people to read
your intimate diary (yes, because it's intimate, you know ...).

Technologies used
-----------------

The language used to create the code is Python 3.

The diary will be encrypted through GPG.

The frontend is using GTK 3 through PyGObject python library.

What is the license of this diary ?
-----------------------------------

The code is under GPL v3 license even if I think RMS uses Emacs for is diary ;-).

